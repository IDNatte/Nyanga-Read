export type MDXMangaEphemeralType = {
  page: number;
  data: Array<any>;
  last: number
};

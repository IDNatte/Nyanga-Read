export type AccountEphemeralType = {
  logged_in: boolean | any,
  user: {
    username: string | undefined,
    email: string | null | undefined
  }
};

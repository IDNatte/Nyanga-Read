export type SearchEphemeralType = {
  keyword?: string,
  local?: Array<any>
};

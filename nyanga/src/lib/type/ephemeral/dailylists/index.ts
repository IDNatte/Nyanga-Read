export type DailyListsEphemeralType = {
	page: number;
	data: Array<any>;
	error?: boolean;
	local?: Array<any>
};

export type ImageViewerType = {
	url?: string | null;
	chapterNumber: number | null;
	index: number | null;
	entity?: "chapter_page" | "end_chapter"
};

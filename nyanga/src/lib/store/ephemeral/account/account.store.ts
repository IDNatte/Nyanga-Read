import { writable } from 'svelte/store';
import type { AccountEphemeralType } from '$lib/type/ephemeral/account';

export default writable<AccountEphemeralType>({ logged_in: false, user: { email: undefined, username: undefined } });

import type { MDXMangaEphemeralType } from '$lib/type/ephemeral/mdxmanga/index'
import { writable } from 'svelte/store';

export default writable<MDXMangaEphemeralType>({ data: [], page: 1, last: 2 });

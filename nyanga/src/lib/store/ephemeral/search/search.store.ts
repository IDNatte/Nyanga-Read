import { writable } from 'svelte/store';
import type { SearchEphemeralType } from '$lib/type/ephemeral/search';

export default writable<SearchEphemeralType>({ keyword: undefined, local: undefined });

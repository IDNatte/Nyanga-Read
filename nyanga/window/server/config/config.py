import pathlib


class Config(object):
    SEND_FILE_MAX_AGE_DEFAULT = 1
    BASEPATH = pathlib.Path.joinpath(pathlib.Path.home(), ".nyanga-read")
    DATABASEPATH = pathlib.Path.joinpath(BASEPATH, "data")

    if not pathlib.Path.exists(BASEPATH):
        pathlib.Path.mkdir(BASEPATH, exist_ok=True)
        pathlib.Path.mkdir(DATABASEPATH, exist_ok=True)

    SQLALCHEMY_DATABASE_URI = f"sqlite:///{DATABASEPATH}/database.db"
    SQLALCHEMY_CONNECT_ARGS = {
        "sqlite_lock_mode": "EXCLUSIVE",
        "check_same_thread": False,
    }


class ExternalConfig(object):
    BASEPATH = pathlib.Path.joinpath(pathlib.Path.home(), ".nyanga-read")
    REQUESTS_CACHE_PATH = pathlib.Path.joinpath(BASEPATH, "cache")

    if not pathlib.Path.exists(REQUESTS_CACHE_PATH):
        pathlib.Path.mkdir(f"{REQUESTS_CACHE_PATH}", exist_ok=True)
        pathlib.Path.mkdir(f"{REQUESTS_CACHE_PATH}/data", exist_ok=True)
        pathlib.Path.mkdir(f"{REQUESTS_CACHE_PATH}/cover", exist_ok=True)
        pathlib.Path.mkdir(f"{REQUESTS_CACHE_PATH}/chapter", exist_ok=True)

    REQUESTS_CACHE_DATA = pathlib.Path.joinpath(REQUESTS_CACHE_PATH, "data")
    REQUESTS_CACHE_COVER = pathlib.Path.joinpath(REQUESTS_CACHE_PATH, "cover")
    REQUESTS_CACHE_CHAPTER = pathlib.Path.joinpath(REQUESTS_CACHE_PATH, "chapter")

    HTTP_REQUESTS_HEADERS = {
        "User-Agent": f"nyanga-desktop/3.0.0 nyanga-client/3.0.0",
    }

    @classmethod
    def http_add_header(cls, key, value):
        cls.HTTP_REQUESTS_HEADERS[key] = value

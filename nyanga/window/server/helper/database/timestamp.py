import datetime


def convert_utmp(unix_timestamp):
    return datetime.datetime.fromtimestamp(unix_timestamp)

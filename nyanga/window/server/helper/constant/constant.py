import os
import tomli

from flask_sqlalchemy import SQLAlchemy
from nyanga.window.utils.resources import get_resources

EXTENSION_CLIENT = "pywebview-client/0.1 pywebview-ext/0.0.1"
PYWEBVIEW_UA = "pywebview-client/1.0 pywebview-ui/3.0.0"
APP_ENV = os.environ.get("APP_ENV", default=None)
DB = SQLAlchemy()

with open(get_resources("config/key.toml"), "rb") as file:
    MDX_CLIENT = tomli.load(file).get("CLIENT_ID")

with open(get_resources("config/key.toml"), "rb") as file:
    MDX_SECRET = tomli.load(file).get("CLIENT_SECRET")

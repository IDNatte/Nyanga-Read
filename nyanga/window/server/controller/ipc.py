import concurrent
import requests
import pathlib
import tomli
import time
import copy
import jwt

from flask import send_from_directory
from flask import request as req
from flask import Blueprint
from flask import redirect
from flask import url_for
from flask import jsonify
from flask import abort

from flask_cors import CORS

from sqlalchemy.exc import IntegrityError as SQLALCHEMYINTEGRITYERROR

from nyanga.window.server.helper.database.timestamp import convert_utmp
from nyanga.window.server.helper.constant import constant

from nyanga.window.server.middleware.verificator.agent import verify_ua
from nyanga.window.server.middleware.verificator.csrf import verify_csrf
from nyanga.window.server.config.config import ExternalConfig

from nyanga.window.server.storage.model.bookmark import Bookmark
from nyanga.window.server.storage.model.settings import Setting
from nyanga.window.server.storage.model.mdx_sync import MDXSync
from nyanga.window.server.storage.model.user import User
from nyanga.window.server.storage.model.read import Read

from nyanga.window.utils.adapter.request import requests as req_adapter

from nyanga.window.utils.resources import get_resources
from nyanga.window.utils.request import parallelize_req
from nyanga.window.utils.request import request_post
from nyanga.window.utils.request import request_get

from nyanga.window.utils.caching import validate_image_cache
from nyanga.window.utils.caching import chapter_caching
from nyanga.window.utils.caching import cover_caching


ipc_handler = Blueprint("ipc_ep", __name__, url_prefix="/ipc")

CORS(ipc_handler)


@ipc_handler.route("/init")
@verify_csrf
@verify_ua
def init():
    try:
        req_adapter().cache.delete(expired=True)
        user_check = User.query.first()

        default_language = (
            Setting.query.filter_by(setting_type="language").one_or_none().to_dict()
        )

        default_content = (
            Setting.query.filter_by(setting_type="content").one_or_none().to_dict()
        )

        default_demographic = (
            Setting.query.filter_by(setting_type="demographic").one_or_none().to_dict()
        )

        content = f"&{("&".join([f"contentRating[]={rating}" for rating, state in default_content.get("value").items() if state.get("active") == True]))}"
        demographic = f"&{("&".join([f"publicationDemographic[]={demographic}" for demographic, state in default_demographic.get("value").items() if state.get("active") == True]))}"

        bookmarks = (
            Bookmark.query.order_by(Bookmark.bookmarked_at.desc()).limit(3).all()
        )

        syncs = MDXSync.query.order_by(MDXSync.sync_at.desc()).limit(3).all()

        latest_chapter = request_get(
            f"https://api.mangadex.org/chapter?includes[]=manga&includeEmptyPages=0&originalLanguage[]=ja&translatedLanguage[]={default_language.get('value').get("lang")}&order[readableAt]=desc{content}",
            cache=False,
        )

        synced_link = [
            f"https://api.mangadex.org/manga/{sync.to_dict().get('manga')}?includes[]=cover_art&includes[]=artist&includes[]=manga"
            for sync in syncs
        ]

        bookmark_link = [
            f"https://api.mangadex.org/manga/{bookmark.to_dict().get('manga')}?includes[]=cover_art&includes[]=artist&includes[]=manga"
            for bookmark in bookmarks
        ]

        if latest_chapter.status_code == 200:
            latest_chapter = latest_chapter.json()

            # use list(set()) to prevent duplication because returning data from tag lists filtering,
            # so there will be duplication data
            # TODO: search better way to filter this thing !!

            chapters_manga_id = list(
                set(
                    [
                        relations.get("id")
                        for chapter in latest_chapter.get("data")
                        for relations in chapter.get("relationships")
                        if relations.get("type") == "manga"
                        for tags in relations.get("attributes").get("tags")
                        if tags.get("id") != "5920b825-4181-4a17-beeb-9918b0ff7a30"
                    ]
                )
            )

            manga_ids = f"?{("&".join([f"ids[]={manga_id}" for manga_id in chapters_manga_id]))}"

            latest_manga = request_get(
                f"https://api.mangadex.org/manga{manga_ids}&includes[]=cover_art&availableTranslatedLanguage[]={default_language.get('value').get("lang")}{demographic}&excludedTags[]=5920b825-4181-4a17-beeb-9918b0ff7a30{content}"
            )
            manga = latest_manga.json()

            with concurrent.futures.ThreadPoolExecutor() as parallelizer:
                fu_sync_manga = [
                    parallelizer.submit(parallelize_req, link) for link in synced_link
                ]

                fu_bookmark = [
                    parallelizer.submit(parallelize_req, link) for link in bookmark_link
                ]

                concurrent.futures.wait(fu_sync_manga + fu_bookmark)

            if user_check:
                try:
                    validate_token = jwt.decode(
                        user_check.token,
                        options={
                            "require": ["iat", "exp", "typ", "aud"],
                            "verify_signature": False,
                            "verify_exp": True,
                        },
                        algorithms=["RS256"],
                    )
                except jwt.exceptions.ExpiredSignatureError as _:
                    constant.DB.session.delete(user_check)
                    constant.DB.session.commit()
                    user_check = None

            return jsonify(
                {
                    "user": (
                        {
                            "username": (
                                validate_token.get("preferred_username")
                                if validate_token
                                else None
                            ),
                            "email": (
                                validate_token.get("email")
                                if validate_token.get("email_verified")
                                else None
                            ),
                        }
                        if user_check != None
                        else None
                    ),
                    "logged_in": True if user_check else False,
                    "daily_manga": [
                        {**manga_content, **{"chapterInfo": chapter.get("attributes")}}
                        for chapter in latest_chapter.get("data")
                        for relationship in chapter.get("relationships")
                        if relationship.get("type") == "manga"
                        for manga_content in manga.get("data")
                        if manga_content.get("id") == relationship.get("id")
                    ],
                    "synced": {
                        "more": True if MDXSync.query.count() > 3 else False,
                        "synced_manga": [
                            sync.result().get("data") for sync in fu_sync_manga
                        ],
                    },
                    "bookmark": {
                        "more": True if Bookmark.query.count() > 3 else False,
                        "bookmark_manga": [
                            data.result().get("data") for data in fu_bookmark
                        ],
                    },
                }
            )

        else:
            return jsonify({"status": "error", "httpError": True})

    except (
        requests.exceptions.ConnectionError,
        requests.exceptions.ConnectTimeout,
        requests.exceptions.Timeout,
    ) as _:
        return jsonify({"status": "error", "location": "exception"})


@ipc_handler.route("/img/cover/<manga>/<filename>")
@verify_ua
def cover_image(manga, filename):
    try:
        cover_cache_path = pathlib.Path.joinpath(
            ExternalConfig.REQUESTS_CACHE_COVER, manga
        )

        cover_cache_file_path = pathlib.Path.joinpath(cover_cache_path, filename)

        cover_img_validator = validate_image_cache(cover_cache_path)

        if cover_img_validator.get("file_count") == 0:
            with concurrent.futures.ThreadPoolExecutor() as parallelizer:
                fu_cover = parallelizer.submit(
                    cover_caching, manga, cover_cache_path, filename
                )

            if fu_cover.result():
                return send_from_directory(cover_cache_path, filename)

        if cover_img_validator.get("file_count") != 0:
            return send_from_directory(cover_cache_path, filename)

    except (
        requests.exceptions.ConnectionError,
        requests.exceptions.ConnectTimeout,
        requests.exceptions.Timeout,
    ) as _:
        return jsonify({"status": "error", "location": "exception"})


@ipc_handler.route("/manga_list")
@verify_csrf
@verify_ua
def manga_lists():
    try:
        default_language = (
            Setting.query.filter_by(setting_type="language").one_or_none().to_dict()
        )

        default_content = (
            Setting.query.filter_by(setting_type="content").one_or_none().to_dict()
        )

        default_demographic = (
            Setting.query.filter_by(setting_type="demographic").one_or_none().to_dict()
        )

        content = f"&{("&".join([f"contentRating[]={rating}" for rating, state in default_content.get("value").items() if state.get("active") == True]))}"
        demographic = f"&{("&".join([f"publicationDemographic[]={demographic}" for demographic, state in default_demographic.get("value").items() if state.get("active") == True]))}"

        in_local_sync = [
            manga.manga for manga in MDXSync.query.all() if len(MDXSync.query.all()) > 0
        ]
        in_bookmark = [
            manga.manga
            for manga in Bookmark.query.all()
            if len(Bookmark.query.all()) > 0
        ]

        already_local = list(set(in_bookmark + in_local_sync))

        page = req.args.get("page", None)
        if page:
            limit = 9
            offset = (int(page) - 1) * int(limit)

            chapter_lists = request_get(
                f"https://api.mangadex.org/chapter?includes[]=manga&includeEmptyPages=0&originalLanguage[]=ja&translatedLanguage[]={default_language.get('value').get("lang")}&order[readableAt]=desc&limit={limit}&offset={offset}{content}",
                cache=False,
            )

            if chapter_lists.status_code == 200:
                latest_chapter = chapter_lists.json()

                # use list(set()) to prevent duplication because returning data from tag lists filtering,
                # so there will be duplication data
                # TODO: search better way to filter this thing !!

                chapters_manga_id = list(
                    set(
                        [
                            relations.get("id")
                            for chapter in latest_chapter.get("data")
                            for relations in chapter.get("relationships")
                            if relations.get("type") == "manga"
                            for tags in relations.get("attributes").get("tags")
                            if tags.get("id") != "5920b825-4181-4a17-beeb-9918b0ff7a30"
                        ]
                    )
                )

                manga_ids = f"?{("&".join([f"ids[]={manga_id}" for manga_id in chapters_manga_id]))}"

                latest_manga = request_get(
                    f"https://api.mangadex.org/manga{manga_ids}&includes[]=cover_art&availableTranslatedLanguage[]={default_language.get('value').get("lang")}{demographic}&excludedTags[]=5920b825-4181-4a17-beeb-9918b0ff7a30{content}"
                )
                manga = latest_manga.json()

                return jsonify(
                    {
                        "daily_mangalists": [
                            {
                                **manga_content,
                                **{"chapterInfo": chapter.get("attributes")},
                            }
                            for chapter in latest_chapter.get("data")
                            for relationship in chapter.get("relationships")
                            if relationship.get("type") == "manga"
                            for manga_content in manga.get("data")
                            if manga_content.get("id") == relationship.get("id")
                        ],
                        "local": already_local,
                    }
                )

            else:
                return jsonify({"status": "error", "httpError": True})

        chapter_lists = request_get(
            f"https://api.mangadex.org/chapter?includes[]=manga&includeEmptyPages=0&originalLanguage[]=ja&translatedLanguage[]={default_language.get('value').get("lang")}&order[readableAt]=desc&limit=9{content}",
            cache=False,
        )

        if chapter_lists.status_code == 200:
            latest_chapter = chapter_lists.json()

            # use list(set()) to prevent duplication because returning data from tag lists filtering,
            # so there will be duplication data
            # TODO: search better way to filter this thing !!

            chapters_manga_id = list(
                set(
                    [
                        relations.get("id")
                        for chapter in latest_chapter.get("data")
                        for relations in chapter.get("relationships")
                        if relations.get("type") == "manga"
                        for tags in relations.get("attributes").get("tags")
                        if tags.get("id") != "5920b825-4181-4a17-beeb-9918b0ff7a30"
                    ]
                )
            )

            manga_ids = f"?{("&".join([f"ids[]={manga_id}" for manga_id in chapters_manga_id]))}"

            latest_manga = request_get(
                f"https://api.mangadex.org/manga{manga_ids}&includes[]=cover_art&availableTranslatedLanguage[]={default_language.get('value').get("lang")}{demographic}&excludedTags[]=5920b825-4181-4a17-beeb-9918b0ff7a30{content}"
            )
            manga = latest_manga.json()

            return jsonify(
                {
                    "daily_mangalists": [
                        {**manga_content, **{"chapterInfo": chapter.get("attributes")}}
                        for chapter in latest_chapter.get("data")
                        for relationship in chapter.get("relationships")
                        if relationship.get("type") == "manga"
                        for manga_content in manga.get("data")
                        if manga_content.get("id") == relationship.get("id")
                    ],
                    "local": already_local,
                }
            )

        else:
            return jsonify({"status": "error", "httpError": True})

    except (
        requests.exceptions.ConnectionError,
        requests.exceptions.ConnectTimeout,
        requests.exceptions.Timeout,
    ) as _:
        return jsonify({"status": "error", "location": "exception"})


@ipc_handler.route("/get_detail/<manga>")
@verify_csrf
@verify_ua
def get_manga_detail(manga):
    try:
        page = req.args.get("page", None)

        bookmarked = Bookmark.query.filter_by(manga=manga).one_or_none()
        latest_readed = (
            Read.query.filter_by(manga=manga).order_by(Read.readed_at.desc()).first()
        )
        language = (
            Setting.query.filter_by(setting_type="language").one_or_none().to_dict()
        )
        default_content = (
            Setting.query.filter_by(setting_type="content").one_or_none().to_dict()
        )
        default_demographic = (
            Setting.query.filter_by(setting_type="demographic").one_or_none().to_dict()
        )

        content = f"&{("&".join([f"contentRating[]={rating}" for rating, state in default_content.get("value").items() if state.get("active") == True]))}"
        demographic = f"&{("&".join([f"publicationDemographic[]={demographic}" for demographic, state in default_demographic.get("value").items() if state.get("active") == True]))}"

        detail = request_get(
            f"https://api.mangadex.org/manga/{manga}?includes[]=cover_art&includes[]=artist&includes[]=manga{content}{demographic}"
        )

        if page:
            limit = 100
            offset = (int(page) - 1) * int(limit)
            detail_manga = request_get(
                f"https://api.mangadex.org/manga/{manga}/feed?includes[]=scanlation_group&includes[]=user&translatedLanguage[]={language.get('value').get("lang")}&order[chapter]=desc&limit={limit}&offset={offset}{content}"
            )

        else:
            detail_manga = request_get(
                f"https://api.mangadex.org/manga/{manga}/feed?includes[]=scanlation_group&includes[]=user&translatedLanguage[]={language.get('value').get("lang")}&order[chapter]=desc{content}"
            )

        if detail.status_code == 200 and detail_manga.status_code == 200:
            detail_manga_data = detail_manga.json()

            return jsonify(
                {
                    "max_page": (
                        (detail_manga_data.get("total") // 100)
                        if detail_manga_data.get("total") % 100 != 0
                        else None
                    ),
                    "paginated": (
                        True if len(detail_manga_data.get("data")) >= 100 else False
                    ),
                    "last_read": latest_readed.to_dict() if latest_readed else None,
                    "detail_data": detail.json().get("data"),
                    "bookmarked": True if bookmarked is not None else False,
                    "manga_data": [
                        {
                            "scanlation": [
                                x.get("attributes")
                                for x in chapter.get("relationships")
                                if x.get("type") == "scanlation_group"
                            ],
                            "user": [
                                x.get("attributes").get("username")
                                for x in chapter.get("relationships")
                                if x.get("type") == "user"
                            ],
                            "language": chapter.get("attributes").get(
                                "translatedLanguage"
                            ),
                            "published": chapter.get("attributes").get("publishAt"),
                            "chapter_id": chapter.get("id"),
                            "chapter": chapter.get("attributes").get("chapter"),
                            "title": chapter.get("attributes").get("title"),
                            # "next_chapter": (
                            #     detail_manga_data.get("data")[
                            #         (len(detail_manga_data.get("data")) - index)
                            #     ].get("id")
                            #     if (len(detail_manga_data.get("data")) - index)
                            #     < len(detail_manga_data.get("data"))
                            #     else None
                            # ),
                            "next_chapter": (
                                detail_manga_data.get("data")[index - 1].get("id")
                                if (len(detail_manga_data.get("data")) - index)
                                < len(detail_manga_data.get("data"))
                                else None
                            ),
                        }
                        for index, chapter in enumerate(detail_manga_data.get("data"))
                    ],
                }
            )

        else:
            return jsonify({"status": "error", "httpError": True})

    except (
        requests.exceptions.ConnectionError,
        requests.exceptions.ConnectTimeout,
        requests.exceptions.Timeout,
    ) as _:
        return jsonify({"status": "error", "location": "exception"})


@ipc_handler.route("/read/<manga>/<chapter>")
@verify_csrf
@verify_ua
def read_chapter(manga, chapter):
    try:
        chapter_cache_path = pathlib.Path.joinpath(
            ExternalConfig.REQUESTS_CACHE_CHAPTER, manga
        )

        chapter_data = request_get(f"https://api.mangadex.org/at-home/server/{chapter}")

        chapter_meta = request_get(
            f"https://api.mangadex.org/chapter/{chapter}?includes[]=manga"
        )

        preload_chapter = (
            Setting.query.filter_by(setting_type="preload_chapter")
            .one_or_none()
            .to_dict()
        )

        if chapter_data.status_code == 200 and chapter_meta.status_code == 200:
            # get chapter detail
            chapter_detail = chapter_meta.json().get("data")
            chapter_detail_manga = next(
                filter(
                    lambda x: x.get("type") == "manga",
                    chapter_detail.get("relationships"),
                )
            ).get("id")
            chapter_detail_id = chapter_detail.get("id")
            chapter_detail_number = chapter_detail.get("attributes").get("chapter")

            # save_read_history
            try:
                save_read_history = Read(
                    manga=chapter_detail_manga,
                    chapter=chapter_detail_id,
                    chapter_number=chapter_detail_number,
                )
                constant.DB.session.add(save_read_history)
                constant.DB.session.commit()

            except SQLALCHEMYINTEGRITYERROR as _:
                pass

            # exchange chapter image
            chapter_image = chapter_data.json()
            base_url = chapter_image.get("baseUrl")
            chapter_hash = chapter_image.get("chapter").get("hash")
            chapter_img_path = chapter_image.get("chapter").get("data")

            # experimental caching chapter
            chapter_cache_file_path = pathlib.Path.joinpath(
                chapter_cache_path, chapter_detail_id
            )

            if validate_image_cache(
                pathlib.Path.joinpath(chapter_cache_path, chapter_detail_id)
            ).get("file_count") < len(chapter_img_path):
                pathlib.Path.mkdir(chapter_cache_path, exist_ok=True)
                pathlib.Path.mkdir(
                    pathlib.Path.joinpath(chapter_cache_path, chapter_detail_id),
                    exist_ok=True,
                )

                url = [
                    {
                        "url": f"{base_url}/data/{chapter_hash}/{chapter_filename}",
                        "filename": chapter_filename,
                    }
                    for chapter_filename in chapter_img_path
                ]

                fu_caching_chapter = []

                with concurrent.futures.ThreadPoolExecutor(
                    max_workers=preload_chapter.get("value").get("preload")
                ) as parallelizer:
                    for i in range(
                        0, len(url), preload_chapter.get("value").get("preload")
                    ):

                        batch = url[i : i + preload_chapter.get("value").get("preload")]

                        fu_caching_chapter.extend(
                            parallelizer.submit(
                                chapter_caching,
                                chapter.get("url"),
                                chapter_cache_file_path,
                                chapter.get("filename"),
                            )
                            for chapter in batch
                        )

                    return jsonify(
                        [
                            {
                                "url": f'http://localhost:5000{url_for(
                                    "ipc_ep.chapter_image",
                                    filename=data.result(),
                                    chapter=chapter_detail_id,
                                    manga=manga,
                                )}',
                                "chapterNumber": int(index + 1),
                                "index": int(index),
                                "entity": "chapter_page",
                            }
                            for index, data in enumerate(
                                concurrent.futures.as_completed(fu_caching_chapter)
                            )
                        ]
                    )

            if validate_image_cache(
                pathlib.Path.joinpath(chapter_cache_path, chapter_detail_id)
            ).get("file_count") == len(chapter_img_path):

                return jsonify(
                    [
                        {
                            "url": f'http://localhost:5000{url_for(
                                "ipc_ep.chapter_image",
                                filename=data,
                                chapter=chapter_detail_id,
                                manga=manga,
                            )}',
                            "chapterNumber": int(index + 1),
                            "index": int(index),
                            "entity": "chapter_page",
                        }
                        for index, data in enumerate(chapter_img_path)
                    ]
                )

        else:
            return jsonify({"status": "error", "httpError": True})

    except (
        requests.exceptions.ConnectionError,
        requests.exceptions.ConnectTimeout,
        requests.exceptions.Timeout,
    ) as _:
        return jsonify({"status": "error", "location": "exception"})


@ipc_handler.route("/img/chapter/<manga>/<chapter>/<filename>")
@verify_ua
def chapter_image(manga, chapter, filename):
    chapter_cache_path = pathlib.Path.joinpath(
        ExternalConfig.REQUESTS_CACHE_CHAPTER, manga
    )

    chapter_cache_file_path = pathlib.Path.joinpath(chapter_cache_path, chapter)

    return send_from_directory(
        pathlib.Path.joinpath(chapter_cache_path, chapter_cache_file_path),
        filename,
    )


@ipc_handler.route("/search")
@verify_csrf
@verify_ua
def search():
    try:
        search_title = req.args.get("search", None)

        default_language = (
            Setting.query.filter_by(setting_type="language").one_or_none().to_dict()
        )

        default_content = (
            Setting.query.filter_by(setting_type="content").one_or_none().to_dict()
        )

        default_demographic = (
            Setting.query.filter_by(setting_type="demographic").one_or_none().to_dict()
        )

        in_local_sync = [
            manga.manga for manga in MDXSync.query.all() if len(MDXSync.query.all()) > 0
        ]
        in_bookmark = [
            manga.manga
            for manga in Bookmark.query.all()
            if len(Bookmark.query.all()) > 0
        ]

        content = f"&{("&".join([f"contentRating[]={rating}" for rating, state in default_content.get("value").items() if state.get("active") == True]))}"
        demographic = f"&{("&".join([f"publicationDemographic[]={demographic}" for demographic, state in default_demographic.get("value").items() if state.get("active") == True]))}"
        already_local = list(set(in_bookmark + in_local_sync))

        if search_title:
            search_manga = request_get(
                f"https://api.mangadex.org/manga?title={search_title}&originalLanguage[]=ja&includes[]=cover_art&availableTranslatedLanguage[]={default_language.get('value').get("lang")}&excludedTags[]=5920b825-4181-4a17-beeb-9918b0ff7a30&limit=9{content}{demographic}",
                cache=False,
            )

            if search_manga.status_code == 200:
                return jsonify(
                    {"search_result": search_manga.json(), "local": already_local}
                )

            else:
                return jsonify({"status": "error", "httpError": True})
        else:
            return jsonify({"result": None})
    except (
        requests.exceptions.ConnectionError,
        requests.exceptions.ConnectTimeout,
        requests.exceptions.Timeout,
    ) as _:
        return jsonify({"status": "error", "location": "exception"})


@ipc_handler.route("/login", methods=["GET", "POST"])
@verify_csrf
@verify_ua
def account_login():
    if req.method == "POST":
        user_payload = req.get_json()

        if user_payload is not None:
            username = user_payload.get("username", None)
            password = user_payload.get("password", None)

            if username and password:
                get_user = request_post(
                    "https://auth.mangadex.org/realms/mangadex/protocol/openid-connect/token",
                    data={
                        "grant_type": "password",
                        "username": username,
                        "password": password,
                        "client_id": constant.MDX_CLIENT,
                        "client_secret": constant.MDX_SECRET,
                    },
                )

                token = get_user.json()
                if get_user.status_code == 200:
                    session_decoder = jwt.decode(
                        token.get("access_token"),
                        options={
                            "require": ["iat", "exp", "typ", "aud"],
                            "verify_signature": False,
                            "verify_exp": True,
                        },
                        algorithms=["RS256"],
                    )

                    user_data = User(
                        token=token.get("access_token"),
                        token_exp=convert_utmp(session_decoder.get("exp")),
                        user_id=session_decoder.get("sub"),
                    )

                    constant.DB.session.add(user_data)
                    constant.DB.session.commit()

                    return jsonify(
                        {
                            "login": True,
                            "user": {
                                "username": session_decoder.get("preferred_username"),
                                "email": (
                                    session_decoder.get("email")
                                    if session_decoder.get("email_verified")
                                    else None
                                ),
                            },
                        }
                    )

                else:
                    return (
                        jsonify(
                            {
                                "status": get_user.json()
                                .get("errors")[0]
                                .get("status"),
                                "message": get_user.json()
                                .get("errors")[0]
                                .get("detail"),
                            }
                        ),
                        get_user.status_code,
                    )

            else:
                return (
                    jsonify(
                        {
                            "status": "error",
                            "message": "username or password is not provided",
                        }
                    ),
                    401,
                )

        else:
            return (
                jsonify(
                    {
                        "status": "error",
                        "code": e.code,
                        "message": str(e).split(":", 1)[1].strip(),
                    }
                )
            ), e.code

    else:
        return (
            jsonify(
                {
                    "status": "error",
                    "message": f"no {req.method} operation accepted",
                }
            ),
            405,
        )


@ipc_handler.route("/logout")
@verify_csrf
@verify_ua
def account_logout():
    user = User.query.first()
    constant.DB.session.delete(user)
    constant.DB.session.commit()
    return jsonify({"login": False})


@ipc_handler.route("/mdxsync", methods=["GET", "POST"])
@verify_csrf
@verify_ua
def sync_mangadex():
    if req.method == "GET":
        page = req.args.get("page", None)
        if page:
            sync_data = MDXSync.query.order_by(MDXSync.sync_at.desc()).paginate(
                page=int(page), per_page=9
            )

            sync_link = [
                f"https://api.mangadex.org/manga/{sync.to_dict().get('manga')}?includes[]=cover_art&includes[]=artist&includes[]=manga"
                for sync in sync_data.items
            ]

            with concurrent.futures.ThreadPoolExecutor() as parallelizer:
                fu = [parallelizer.submit(parallelize_req, link) for link in sync_link]
                concurrent.futures.wait(fu)

            return jsonify(
                {
                    "sync_mdx": [data.result().get("data") for data in fu],
                    "page": {"current": sync_data.page, "last": sync_data.pages},
                }
            )

        if page is None:
            sync_data = MDXSync.query.order_by(MDXSync.sync_at.desc()).paginate(
                page=1, per_page=9
            )
            sync_link = [
                f"https://api.mangadex.org/manga/{sync.to_dict().get('manga')}?includes[]=cover_art&includes[]=artist&includes[]=manga"
                for sync in sync_data.items
            ]

            with concurrent.futures.ThreadPoolExecutor() as parallelizer:
                fu = [parallelizer.submit(parallelize_req, link) for link in sync_link]
                concurrent.futures.wait(fu)

            return jsonify(
                {
                    "sync_mdx": [data.result().get("data") for data in fu],
                    "page": {"current": int(sync_data.page), "last": sync_data.pages},
                }
            )

    if req.method == "POST":
        user = User.query.first().to_dict()
        sync_local_data = [
            local_sync_data.manga
            for local_sync_data in MDXSync.query.all()
            if len(MDXSync.query.all()) > 0
        ]

        local_bookmark_data = [
            local_bookmark.manga
            for local_bookmark in Bookmark.query.all()
            if len(Bookmark.query.all()) > 0
        ]

        try:
            ExternalConfig.http_add_header(
                "Authorization", f"Bearer {user.get('token')}"
            )
            mdx_manga_lists = request_get(
                "https://api.mangadex.org/manga/status",
                headers=ExternalConfig.HTTP_REQUESTS_HEADERS,
            )

            if mdx_manga_lists.status_code == 200:
                sync_cloud = mdx_manga_lists.json()
                sync_cloud_data = sync_cloud.get("statuses").keys()

                sync_cloud_to_local = [
                    MDXSync(manga=manga_id)
                    for manga_id in sync_cloud_data
                    if manga_id not in sync_local_data
                ]

                sync_local_to_cloud = [
                    f"https://api.mangadex.org/manga/{local_data}/status"
                    for local_data in local_bookmark_data
                    if local_data not in sync_cloud_data
                ]

                remove_unsynced_data = [
                    MDXSync.query.filter(MDXSync.manga.in_([local_remove]))
                    for local_remove in sync_local_data
                    if local_remove not in sync_cloud_data
                ]

                if len(remove_unsynced_data) >= 1:
                    for unsync in remove_unsynced_data:
                        unsync.delete()

                    constant.DB.session.commit()

                if len(sync_cloud_to_local) >= 1:
                    constant.DB.session.add_all(sync_cloud_to_local)
                    constant.DB.session.commit()

                if len(sync_local_to_cloud) > 0:
                    with concurrent.futures.ThreadPoolExecutor() as parallelizer:
                        payload = {"status": "reading"}

                        fu_sync_local_bookmark = [
                            parallelizer.submit(
                                request_post,
                                link,
                                headers=ExternalConfig.HTTP_REQUESTS_HEADERS,
                                json=payload,
                            )
                            for link in sync_local_to_cloud
                        ]

                        concurrent.futures.wait(fu_sync_local_bookmark)

                return jsonify({"sync": "done"}), 201

        except (
            requests.exceptions.ConnectionError,
            requests.exceptions.ConnectTimeout,
            requests.exceptions.Timeout,
        ) as _:
            return jsonify({"status": "error", "location": "exception"})


@ipc_handler.route("/bookmark", methods=["GET", "POST", "DELETE"])
@verify_csrf
@verify_ua
def bookmark():
    if req.method == "GET":
        bookmark_data = Bookmark.query.order_by(Bookmark.bookmarked_at.desc()).all()

        bookmark_link = [
            f"https://api.mangadex.org/manga/{bookmark.to_dict().get('manga')}?includes[]=cover_art&includes[]=artist&includes[]=manga"
            for bookmark in bookmark_data
        ]

        with concurrent.futures.ThreadPoolExecutor() as parallelizer:
            fu = [parallelizer.submit(parallelize_req, link) for link in bookmark_link]
            concurrent.futures.wait(fu)

        return jsonify({"bookmark": [data.result().get("data") for data in fu]})
    if req.method == "POST":
        payload = req.get_json()

        if payload is not None or payload:
            if payload.get("mangaId") != None:
                bookmark_save = Bookmark(manga=payload.get("mangaId"))
                constant.DB.session.add(bookmark_save)
                constant.DB.session.commit()

                return {"status": "bookmarked", "message": "Bookmark Saved !"}

            else:
                return jsonify(
                    {
                        "status": "error",
                        "mangaIdError": True,
                        "message": "Something wen't error",
                    }
                )

        else:
            return jsonify(
                {
                    "status": "error",
                    "bodyError": True,
                    "message": "Something wen't error",
                }
            )

    if req.method == "DELETE":
        payload = req.get_json()

        if payload is not None or payload:
            if payload.get("mangaId") != None:
                bookmark_remove = Bookmark.query.filter_by(
                    manga=payload.get("mangaId")
                ).one_or_none()

                constant.DB.session.delete(bookmark_remove)
                constant.DB.session.commit()

                return {"status": "unbookmarked", "message": "Bookmark removed !"}

            else:
                return jsonify(
                    {
                        "status": "error",
                        "mangaIdError": True,
                        "message": "Something wen't error",
                    }
                )

        else:
            return jsonify(
                {
                    "status": "error",
                    "bodyError": True,
                    "message": "Something wen't error",
                }
            )

    return redirect(url_for("main.index"))


@ipc_handler.route("/settings", methods=["GET", "PATCH", "DELETE"])
@verify_csrf
@verify_ua
def settings():
    if req.method == "GET":
        settings = Setting.query.all()
        return jsonify(
            {
                "settings": [setting.to_dict() for setting in settings],
            }
        )

    if req.method == "PATCH":
        update_setting = req.get_json()
        if update_setting:
            settings_id = update_setting.get("setting", None)
            settings_property = update_setting.get("property", None)

            if settings_id and settings_property:
                setting_value = update_setting.get("value", None)
                if not setting_value:
                    update = Setting.query.get(settings_id)
                    update.value = copy.deepcopy(update.value)

                    update.value[settings_property]["active"] = not update.value[
                        settings_property
                    ]["active"]

                    constant.DB.session.add(update)
                    constant.DB.session.commit()
                    return jsonify({"status": "changed", "message": "Settings changed"})

                if setting_value:
                    update = Setting.query.get(settings_id)
                    update.value = copy.deepcopy(update.value)

                    update.value[settings_property] = setting_value

                    constant.DB.session.add(update)
                    constant.DB.session.commit()

                    return jsonify({"status": "changed", "message": "Settings changed"})

            else:
                return jsonify({"status": "unchanged", "message": "Settings unchanged"})

        else:
            return jsonify({"emptySettings": True})

    return redirect(url_for("main.index"))


@ipc_handler.route("/app")
@verify_csrf
@verify_ua
def app_info():
    try:
        data_cache_folder = ExternalConfig.REQUESTS_CACHE_DATA
        cover_cache_folder = ExternalConfig.REQUESTS_CACHE_COVER
        chapter_cache_folder = ExternalConfig.REQUESTS_CACHE_CHAPTER

        data_cache_size = sum(
            f.stat().st_size
            for f in pathlib.Path(data_cache_folder).rglob("*")
            if f.is_file()
        )
        cover_cache_size = sum(
            f.stat().st_size
            for f in pathlib.Path(cover_cache_folder).rglob("*")
            if f.is_file()
        )
        chapter_cache_size = sum(
            f.stat().st_size
            for f in pathlib.Path(chapter_cache_folder).rglob("*")
            if f.is_file()
        )

        with open(get_resources("docs/app/about.md"), "r") as about_app:
            about = about_app.read()

        with open(get_resources("docs/app/VERSION.toml"), "rb") as versioning:
            version = tomli.load(versioning)

        return jsonify(
            {
                "app_info": {
                    "app_about": about,
                    "version": version.get("VERSION"),
                    "build": version.get("BUILD_SHA"),
                    "cache": {
                        "data": data_cache_size / (1024**2),
                        "cover": cover_cache_size / (1024**2),
                        "chapter": chapter_cache_size / (1024**2),
                    },
                }
            }
        )

    except FileNotFoundError as _:
        return jsonify({"status": "error", "location": "exception"})

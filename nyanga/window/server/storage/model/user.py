from sqlalchemy_serializer import SerializerMixin


from nyanga.window.server.helper.constant.constant import DB


class User(DB.Model, SerializerMixin):
    __tablename__ = "nyanga_user"

    id = DB.Column(DB.Integer, primary_key=True)
    token = DB.Column(DB.String, nullable=False, unique=True)
    token_exp = DB.Column(DB.DateTime, nullable=False)
    user_id = DB.Column(DB.String)

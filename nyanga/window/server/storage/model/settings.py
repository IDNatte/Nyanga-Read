from sqlalchemy_serializer import SerializerMixin
from sqlalchemy.ext.mutable import MutableDict
from sqlalchemy.dialects.sqlite import JSON

from nyanga.window.server.helper.constant.constant import DB


class Setting(DB.Model, SerializerMixin):
    __tablename__ = "nyanga_setting"

    id = DB.Column(DB.Integer, primary_key=True)
    setting_type = DB.Column(DB.String(50), nullable=False, unique=True)
    value = DB.Column(MutableDict.as_mutable(JSON), nullable=False)

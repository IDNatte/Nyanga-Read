import datetime
import requests_cache

from requests.adapters import HTTPAdapter
from requests.adapters import BaseAdapter
from urllib3.util.retry import Retry

from nyanga.window.server.config.config import ExternalConfig
from nyanga.window.utils.network.pool_setter import autoset_pool_size


def requests(pool_maxsize=autoset_pool_size()):
    request_sqlite_backend = requests_cache.SQLiteCache(
        db_path=f"{ExternalConfig.REQUESTS_CACHE_DATA}/cached_data.db",
        fast_save=True,
        wal=True,
    )

    # Create a cached session
    session = requests_cache.CachedSession(
        backend=request_sqlite_backend,
        stale_while_revalidate=True,
        stale_if_error=True,
        cache_control=True,
        expire_after=datetime.timedelta(minutes=5).seconds,
    )

    # Create an HTTPAdapter with increased pool size
    adapter = HTTPAdapter(
        pool_connections=pool_maxsize / 2,
        pool_maxsize=pool_maxsize,
        max_retries=Retry(total=10),
    )

    # Mount the adapter to handle HTTP and HTTPS
    session.mount("http://", adapter)
    session.mount("https://", adapter)

    return session

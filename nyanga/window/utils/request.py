from nyanga.window.server.config.config import ExternalConfig
from nyanga.window.utils.adapter.request import requests


def parallelize_req(url, headers=ExternalConfig.HTTP_REQUESTS_HEADERS, cache=None):
    if cache == None or cache == True:
        parallel_executor = requests().get(url, headers=headers, refresh=True)

    if cache == False:
        with requests().cache_disabled():
            parallel_executor = requests().get(url, headers=headers, refresh=True)

    return parallel_executor.json()


def request_get(
    url,
    headers=ExternalConfig.HTTP_REQUESTS_HEADERS,
    stream=None,
    cache=None,
    json=None,
):
    if cache == None or cache == True:
        req = requests().get(url, headers=headers, refresh=True, stream=stream)

    if cache == False:
        with requests().cache_disabled():
            req = requests().get(url, headers=headers, refresh=True, stream=stream)

    if json == True:
        return req.json()

    return req


def request_post(
    url, headers=ExternalConfig.HTTP_REQUESTS_HEADERS, data=None, json=None, cache=None
):
    if cache == None or cache == True:
        req = requests().post(url, headers=headers, data=data, json=json)

    if cache == False:
        with requests().cache_disabled():
            req = requests().post(url, headers=headers, data=data, json=json)

    return req

import requests
import pathlib
import os

from PIL import Image

from nyanga.window.utils.request import request_get


def cover_caching(manga, cover_path, filename):
    try:
        pathlib.Path.mkdir(cover_path, exist_ok=True, parents=True)
        cover = request_get(f"https://uploads.mangadex.org/covers/{manga}/{filename}")
        if cover.status_code == 200:
            with open(f"{cover_path}/{filename}", "wb") as cover_file:
                cover_file.write(cover.content)

            return True

    except (
        requests.exceptions.ConnectionError,
        requests.exceptions.ConnectTimeout,
        requests.exceptions.Timeout,
    ) as _:
        return False


def chapter_caching(url, path, filename):
    try:
        chapter = request_get(url)
        if chapter.status_code == 200:
            with open(f"{path}/{filename}", "wb") as chapter_file:
                chapter_file.write(chapter.content)

            return filename

    except (
        requests.exceptions.ConnectionError,
        requests.exceptions.ConnectTimeout,
        requests.exceptions.Timeout,
    ) as _:
        return False


def __is_image_validator(image_path):
    try:
        img = Image.open(image_path)
        img.verify()
        return True

    except (IOError, SyntaxError):
        return False


def validate_image_cache(path):
    try:
        invalid = []
        file_count = len(
            [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
        )
        for filename in os.listdir(path):
            file_path = os.path.join(path, filename)
            if os.path.isfile(file_path):
                if not __is_image_validator(file_path):
                    invalid.append({"path": file_path, "filename": filename})

        return {"file_count": file_count, "invalid": invalid}

    except FileNotFoundError as _:
        return {"file_count": 0, "invalid": []}

from nyanga.window.server.storage.model.settings import Setting
from nyanga.window.server.helper.constant import constant
from nyanga.window.server.server import create_app


def db_settings_initializer():
    with create_app().app_context():
        # check
        constant.DB.create_all()
        check_db_population = Setting.query.count()

        # initial data
        init_lang_settings = Setting(setting_type="language", value={"lang": "en"})
        init_content_settings = Setting(
            setting_type="content",
            value={
                "safe": {"active": True, "value": "safe"},
                "suggestive": {"active": False, "value": "suggestive"},
                "erotica": {
                    "active": False,
                    "value": "erotica",
                },
                "pornographic": {"active": False, "value": "pornographic"},
            },
        )

        init_demographic_settings = Setting(
            setting_type="demographic",
            value={
                "shounen": {"active": True, "value": "shounen"},
                "shoujo": {"active": True, "value": "shoujo"},
                "josei": {
                    "active": True,
                    "value": "josei",
                },
                "seinen": {"active": True, "value": "seinen"},
                "none": {"active": True, "value": "none"},
            },
        )

        init_chapter_preload = Setting(
            setting_type="preload_chapter", value={"preload": 5}
        )

        if check_db_population == 0:
            constant.DB.session.add(init_lang_settings)
            constant.DB.session.add(init_content_settings)
            constant.DB.session.add(init_demographic_settings)
            constant.DB.session.add(init_chapter_preload)
            constant.DB.session.commit()

        # backward compatibility
        # if check_db_population != 0:
        #     test_db = Setting.query.all()
        #     sample = [isinstance(data.to_dict().get("value"), dict) for data in test]
        #     if False in sample:
        #         constant.DB.session.query(Setting).delete()
        #         constant.DB.session.add(init_lang_settings)
        #         constant.DB.session.add(init_content_settings)
        #         constant.DB.session.add(init_demographic_settings)
        #         constant.DB.session.commit()

        #     # pass

import requests
import ping3
import time


def network_latency():
    latency = ping3.ping("api.mangadex.org", timeout=2)
    latency = latency if latency else 0

    total_time = 0
    for _ in range(1):
        start_time = time.time()
        try:
            response = requests.get("https://api.mangadex.org/ping")
            total_time += time.time() - start_time
        except requests.exceptions.RequestException:
            continue
    response_time = total_time / 1

    return {"latency": latency, "response_time": response_time}

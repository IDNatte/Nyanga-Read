import time
import requests

from ping3 import ping


def autoset_pool_size(desired_concurrent_requests=5, num_requests=5):
    latency = ping("api.mangadex.org", timeout=2)
    latency = latency if latency else 0

    total_time = 0
    for _ in range(num_requests):
        start_time = time.time()
        try:
            response = requests.get("https://api.mangadex.org/ping")
            total_time += time.time() - start_time
        except requests.exceptions.RequestException:
            continue
    response_time = total_time / num_requests if num_requests else 0

    if latency == 0 or response_time == 0:
        return 10

    pool_size = int(desired_concurrent_requests * (response_time / latency))

    return max(pool_size, 10)

# -*- mode: python ; coding: utf-8 -*-

import os

block_cipher = None

binary_name = os.getenv('PYINSTALLER_BINARY_NAME', 'Nyanga-Read-linux')


a = Analysis(
    ['../../main.py'],
    pathex=[],
    binaries=[],
    datas=[
        ("../../layout", "layout"),
        ("../../docs/app", "docs/app"),
        ("../../log", "log"),
        ("../../config", "config")
    ],
    hiddenimports=[
        'desktop_notifier.resources',
    ],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(
    pyz,
    a.scripts,
    a.binaries,
    a.zipfiles,
    a.datas,
    [],
    name=binary_name,
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    upx_exclude=[],
    runtime_tmpdir=None,
    console=False,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
)

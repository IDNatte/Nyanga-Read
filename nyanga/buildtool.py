from pathlib import Path
import subprocess
import os

import PyInstaller.__main__


def start_ui():
    try:
        subprocess.run(
            ["yarn", "run", "svelte:dev"], cwd=Path(__file__).parent.absolute()
        )
    except KeyboardInterrupt:
        pass


def install_ui_dep():
    try:
        subprocess.run(["yarn", "install"], cwd=Path(__file__).parent.absolute())
    except KeyboardInterrupt:
        pass


def build_ui():
    try:
        subprocess.run(["yarn", "install"], cwd=Path(__file__).parent.absolute())
        subprocess.run(
            ["yarn", "run", "svelte:build"], cwd=Path(__file__).parent.absolute()
        )
    except KeyboardInterrupt:
        pass


def build_linux():
    distro = None
    os_get_distro = subprocess.run(
        ["lsb_release", "-si"],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
    )

    if os_get_distro.returncode != 0:
        distro = "linux"

    if os_get_distro.returncode == 0:
        distro = os_get_distro.stdout.strip().lower()

    os.environ["PYINSTALLER_BINARY_NAME"] = f"Nyanga-Read-{distro}"

    PyInstaller.__main__.run(
        [
            f"{Path(__file__).parent.absolute()}/buildtools/specfile/Nyanga-Read-linux.spec",
            "--distpath",
            "nyanga-bin",
        ]
    )


def build_windows():
    PyInstaller.__main__.run(
        [
            f"{Path(__file__).parent.absolute()}/buildtools/specfile/Nyanga-Read-linux.spec",
            "--distpath",
            "nyanga-window",
        ]
    )

<p align="center">
  <!-- <img style="margin: 0px; width: 150px;" src="https://gitlab.com/IDNatte/Nyanga-Read/-/raw/main/docs/images/logo.png" alt="Nyanga-Read"> -->
  <img style="margin: 0px; width: 150px;" src="https://gitlab.com/IDNatte/Nyanga-Read/-/raw/main/nyanga/docs/app/logo.png" alt="Nyanga-Read">

</p>

<p style="margin: 0px;" align="center">
<a href="https://gitlab.com/IDNatte/Nyanga-Read/-/raw/main/LICENSE" target="_blank">
<img alt="GitLab License" src="https://img.shields.io/gitlab/license/IDNatte%2FNyanga-Read?style=for-the-badge&color=86efac">
</a>
</p>

<!-- <p style="margin: 0px;" align="center">
<a href="https://gitlab.com/IDNatte/Nyanga-Read/-/releases" target="_blank">
<img alt="Dynamic TOML Badge" src="https://img.shields.io/badge/dynamic/toml?url=https%3A%2F%2Fgitlab.com%2FIDNatte%2FNyanga-Read%2F-%2Fraw%2Fmain%2Fpyproject.toml&query=%24.tool.poetry.version&style=for-the-badge&label=Nyanga-Read&color=%23f9a8d4&link=https%3A%2F%2Fgitlab.com%2FIDNatte%2FNyanga-Read%2F-%2Freleases">
</a>
</p> -->

---

### list used 3rd party library and API Source by this nyanga-read

#### **UI Framework and library**

- svelte
  - [svelte](https://svelte.dev)
  - [sveltekit](https://kit.svelte.dev/)
  - [svelte-french-toast](https://svelte-french-toast.com/)
  - [svelte-i18n](https://github.com/kaisermann/svelte-i18n)

- tailwind
  - [tailwind](https://tailwindcss.com/)
  - [@tailwindcss/typography](https://tailwindcss.com/docs/typography-plugin)

- lodash
  - [debounce](https://lodash.com/docs/#debounce)
  - [truncate](https://lodash.com/docs/#truncate)
  - [values](https://lodash.com/docs/#values)
  - [find](https://lodash.com/docs/#find)
  - [get](https://lodash.com/docs/#get)

- markdown
  - [showdown](https://showdownjs.com/)

#### **System**

- system
  - [flask](https://flask.palletsprojects.com/en/2.3.x/)
  - [SQLAlchemy](https://www.sqlalchemy.org/)
  - [Python Requests](https://docs.python-requests.org/en/latest/index.html)
  - [request-cache](https://requests-cache.readthedocs.io/en/stable/)
  - [ping3](https://github.com/kyan001/ping3)
  - [desktop-notifier](https://desktop-notifier.readthedocs.io/en/latest/)

- windowing
  - [pywebview](https://pywebview.flowrl.com/)

- builder
  - [pyinstaller](https://pyinstaller.org/en/stable/)

#### **API**

- [mangadex](https://api.mangadex.org/)

Feel free to give any suggestion or request a feature or submit any bug @ [gitlab issue](https://gitlab.com/IDNatte/Nyanga-Read/issues)

---

### Ulauncher extension for nyanga-read ✨.

extend your [ulauncher](https://ulauncher.io/) with [ulauncher-nyanga](https://github.com/IDNatte/ulauncher-nyanga) to use it, go to [ulauncher-nyanga](https://github.com/IDNatte/ulauncher-nyanga) github repo.
